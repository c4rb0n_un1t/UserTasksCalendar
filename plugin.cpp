#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this),
	m_userTasks(),
	m_impl(new UserTasksCalendar(this, m_userTasks))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this}
		, {INTERFACE(IUserTasksCalendar), m_impl}
	},
	{
		{INTERFACE(IUserTaskDateDataExtention), m_userTasks}
	},
	{}
	);
}

void Plugin::onReady()
{
	m_impl->init();
}
