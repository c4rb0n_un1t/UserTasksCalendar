#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Utility/i_user_task_date_data_ext.h"
#include "../../Interfaces/Utility/iusertaskscalendar.h"
#include "usertaskscalendar.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.UserTasksCalendar" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	virtual ~Plugin() = default;

	// PluginBase interface
protected:
	void onReady() override;

private:
	ReferenceInstancePtr<IUserTaskDateDataExtention> m_userTasks;
	UserTasksCalendar* m_impl;
};
